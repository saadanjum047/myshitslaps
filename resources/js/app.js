/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Swal from 'sweetalert2'
window.Swal = Swal

import Sweel from 'sweetalert'
window.Sweel = Sweel


import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

// var toolbarOptions = ['bold', 'italic', 'underline', 'strike'];

var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],
  
    // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    // [{ 'direction': 'rtl' }],                         // text direction
  
    // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
    // [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    // [{ 'font': [] }],
    [{ 'align': [] }],
  
    // ['clean']                                         // remove formatting button
  ];
  

Vue.use(VueQuillEditor,  {
    modules: {
      toolbar: toolbarOptions
    }})


import FlashMessage from '@smartweb/vue-flash-message';
Vue.use(FlashMessage);
    


import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
let toastr = require('toastr')
toastr.options.progressBar = true;
toastr.options.timeOut = 0;
window.toastr = toastr

Vue.use(VueToastr2)


import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'
Vue.component('v-select', vSelect)


import uploader from 'vue-simple-uploader'
Vue.use(uploader)
window.uploader = uploader



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);


Vue.component('view-product', require('./components/public/view-product.vue').default);
Vue.component('cart', require('./components/public/cart.vue').default);
Vue.component('checkout', require('./components/public/checkout.vue').default);
Vue.component('contact', require('./components/public/contact.vue').default);
Vue.component('order-details', require('./components/public/order-details.vue').default);
Vue.component('mailing-component', require('./components/public/mailing.vue').default);


Vue.component('product-form', require('./components/admin/productForm.vue').default);
Vue.component('loader', require('./components/loader.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
