@extends('layouts.admin')


@section('content')


    <div class="row orders-view-page">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">View Queries</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Order Date</th>
                            <th>Actions</th>
                        </tr>

                        @if(isset($queries))
                            @foreach($queries as $k => $query)
                                <tr>
                                    {{--  <td> {{$k+1}} </td>  --}}
                                    <td> {{ $k + 1 + $queries->perPage() * ($queries->currentPage() - 1)}} </td>
                                    <td> {{ ucwords ($query->first_name .  ' ' .$query->last_name)}} </td>
                                    <td> {{$query->email}} </td>
                                    <td>
                                        @if($query->resolved == 1)
                                        <span class="badge badge-success">Resolved</span>
                                        @else
                                        <span class="badge badge-danger">Pending</span>
                                        @endif
                                    </td>
                                    <td> {{$query->created_at->format('m/d/Y') }} </td>
                                    <td>
                                        @if($query->resolved != 1)
                                        <a href="{{route('admin.query.resolve' , $query->id)}}" class="text-success" title="Mark as Resolved" > <i class="fas fa-check"></i> </a>
                                        @endif

                                        <a href="{{route('admin.query.show' , $query->id)}}" class="text-primary" title="View Query" > <i class="fas fa-eye"></i> </a>
                                        
                                    </td>

                                </tr>
                            @endforeach
                        @endif
                    </table>

                    {{$queries->links()}}

                </div>
            </div>

        </div>
    </div>

@endsection