@extends('layouts.admin')

@push('style')
<style>
    .item-photo{
        width: auto;
        margin-left: auto;
        margin-right: auto;
        height: 90px;
        width: 125px;
        border-radius: 5px;
        object-fit: cover;
        {{--  height: 75px;
        max-width: 100%;  --}}
    }

    .full-center{
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-row{
        border-top: 1px solid #ececec;
        padding-top: 10px;
        margin: 0 5px;
    }
</style>

@endpush

@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title ">Query Details</h3>
                    @if($query->resolved != 1)
                    <a class="btn btn-success float-right" href="{{ route('admin.query.resolve' , $query->id) }}" >Resolve</a>
                    @endif
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <div class="row">
                        <div class="col-3">
                            <label>Customer Name:</label>
                        </div>
                        <div class="col-3">
                            <label>{{ ucwords($query->first_name . " " . $query->last_name)}}</label>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-3">
                            <label>Customer Email:</label>
                        </div>
                        <div class="col-3">
                            <label>{{$query->email}}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label>Date:</label>
                        </div>
                        <div class="col-3">
                            <label>{{ $query->created_at->toDateString() }} </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label>Status:</label>
                        </div>
                        <div class="col-3">
                            @if($query->resolved == 1)
                            <label class="badge badge-success">Resolved</label>
                            @else
                            <label class="badge badge-danger">Pending</label>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label>Message:</label>
                        </div>
                        <div class="col-9">
                            <label>{{ $query->message }} </label>
                        </div>
                    </div>                  
                </div>
            </div>

        </div>
    </div>

@endsection
