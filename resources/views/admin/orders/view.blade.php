@extends('layouts.admin')

@push('style')
<style>
    .item-photo{
        width: auto;
        margin-left: auto;
        margin-right: auto;
        height: 90px;
        width: 125px;
        border-radius: 5px;
        object-fit: cover;
        {{--  height: 75px;
        max-width: 100%;  --}}
    }

    .full-center{
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .item-row{
        border-top: 1px solid #ececec;
        padding-top: 10px;
        margin: 0 5px;
    }
</style>

@endpush

@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title ">Order Details</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <div class="row">
                        <div class="col-3">
                            <label>Customer Name:</label>
                        </div>
                        <div class="col-3">
                            <label>{{ ucwords($order->first_name . " " . $order->last_name)}}</label>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-3">
                            <label>Customer Email:</label>
                        </div>
                        <div class="col-3">
                            <label>{{$order->email}}</label>
                        </div>
                    </div>
                    @if($order->payment_method != 'Free' )
                    
                    <div class="row">
                        <div class="col-3">
                            <label>Amount:</label>
                        </div>
                        <div class="col-3">
                            <label> $ {{ $order->total_amount }} </label>
                            {{--  <label> $ {{ array_sum($order->items->pluck('price')->toArray()) }} </label>  --}}
                        </div>
                    </div>
                    @if(isset($order->coupen))
                    <div class="row">
                        <div class="col-3">
                            <label>Discount Availed:</label>
                        </div>
                        <div class="col-3">
                            <label>{{ $order->coupen->discount }}% (${{ $order->amount }}) </label>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-3">
                            <label>Coupen Used:</label>
                        </div>
                        <div class="col-3">
                            <label>{{ isset($order->coupen) ? $order->coupen->name : "No"  }} </label>
                        </div>
                        <div class="col-3">
                            <label>{{ isset($order->coupen) ? $order->coupen->code : ""  }} </label>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-3">
                            <label>Payment Method:</label>
                        </div>
                        <div class="col-3">
                            <label>{{ ucwords($order->payment_method) }} </label>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-3">
                            <label>Date:</label>
                        </div>
                        <div class="col-3">
                            <label>{{ $order->created_at->toDateString() }} </label>
                        </div>
                    </div>
                    
                    <hr>
                    @if($order->items)
                    <h4 class="mb-2">Item Details</h4>
                    @foreach($order->items as $item)
                    <div class="row mb-3 item-row">
                        <div class="col-2 text-center">
                            @if($item->image)
                            <img class="item-photo" src="/storage/products/{{$item->image}}" />
                            @else
                            <img class="item-photo" src="/images/template.jpg" />
                            @endif
                        </div>
                        <div class="col-2 full-center">
                            <label>{{ $item->title }} </label>
                        </div>
                        
                        <div class="col-4 full-center">
                            @if($item->category)
                            <label>{{ $item->category->name }} </label>
                            @endif
                        </div>
                        <div class="col-2 full-center">
                            <label>{{ $item->is_free ? 'Free' : '$'.$item->price }} </label>
                        </div>
                        {{--  @dump( $item , $item->total_price != $item->price)  --}}
                        @if($item->total_price != $item->price)
                            <div class="col-2 full-center">
                                <label>${{ $item->total_price - $item->price }} (Discount) </label>
                            </div>
                        @endif
                    </div>
                    @endforeach
                    @endif
                    
                </div>
            </div>

        </div>
    </div>

@endsection
