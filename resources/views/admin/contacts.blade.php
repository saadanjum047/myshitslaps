@extends('layouts.admin')


@section('content')


    <div class="row orders-view-page">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">View Queries</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Order Date</th>
                            <th>Actions</th>
                        </tr>

                        @if(isset($orders))
                            @foreach($orders as $k => $order)
                                <tr>
                                    {{--  <td> {{$k+1}} </td>  --}}
                                    <td> {{ $k + 1 + $orders->perPage() * ($orders->currentPage() - 1)}} </td>
                                    <td> {{ ucwords ($order->first_name .  ' ' .$order->last_name)}} </td>
                                    <td> {{$order->email}} </td>
                                    <td> {{ $order->total_amount > 0 ? '$' . $order->total_amount : "Free" }} </td>

                                    <td> {{$order->created_at->format('m/d/Y') }} </td>
                                    <td>
                                        <a href="{{route('admin.orders.show' , $order->id)}}" class="text-success"  > <i class="fas fa-eye"></i> </a>
                                        
                                        <a href="{{route('admin.orders.delete' , $order->id)}}" > <i class="fas fa-trash text-danger"></i> </a>
                                    </td>

                                    {{--  <form id="delete-form-{{$order->id}}" action="{{route('admin.category.destroy' , $order->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>  --}}
                                </tr>
                            @endforeach
                        @endif
                    </table>

                    {{$orders->links()}}

                </div>
            </div>

        </div>
    </div>

@endsection