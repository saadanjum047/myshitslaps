@extends('layouts.admin')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">Manage Categories</h3>
                    <a class="btn btn-outline-primary btn-sm float-right" href="{{route('admin.category.create')}}"> <i class="fas fa-plus"></i> Create</a>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Create Date</th>
                            <th>Actions</th>
                        </tr>
                        @if(isset($categories))
                            @foreach($categories as $k => $category)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> {{$category->name}} </td>

                                    <td> {{$category->created_at->format('m/d/Y') }} </td>
                                    <td>


                                        <a href="javascript:;" onclick="AskBeforeDelete({{$category->id}} , 'delete-form-{{$category->id}}')" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                                        <a href="{{route('admin.category.edit' , $category->id)}}" > <i class="fas fa-edit"></i> </a>
                                    </td>

                                    <form id="delete-form-{{$category->id}}" action="{{route('admin.category.destroy' , $category->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection