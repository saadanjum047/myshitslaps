@extends('layouts.admin')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">Manage Coupens</h3>
                    <a class="btn btn-outline-primary btn-sm float-right" href="{{route('admin.coupen.create')}}"> <i class="fas fa-plus"></i> Create</a>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Validity</th>
                            <th>Status</th>
                            <th>Create Date</th>
                            <th>Actions</th>
                        </tr>
                        @if(isset($coupens))
                            @foreach($coupens as $k => $coupen)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> {{$coupen->name}} </td>
                                    <td> {{$coupen->code}} </td>
                                    <td> {{$coupen->validity_date}} </td>
                                    <td> @if($coupen->validity) @if($coupen->isValid) <span class="badge badge-success">Active</span> @else <span class="badge badge-danger">Expired</span>@endif @else <span class="badge badge-success">Active</span> @endif </td>

                                    <td> {{$coupen->created_at->format('m/d/Y') }} </td>
                                    <td class="text-align">


                                        <a href="javascript:;" onclick="AskBeforeDelete({{$coupen->id}} , 'delete-form-{{$coupen->id}}')" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                                        {{--  <a href="{{route('admin.coupen.edit' , $coupen->id)}}" > <i class="fas fa-edit"></i> </a>  --}}
                                    </td>

                                    <form id="delete-form-{{$coupen->id}}" action="{{route('admin.coupen.destroy' , $coupen->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </tr>
                            @endforeach
                        @endif
                    </table>

                    {{$coupens->links()}}
                </div>
            </div>

        </div>
    </div>

@endsection