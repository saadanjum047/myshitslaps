@extends('layouts.admin')

@section('content')

{{--  <h1>ADMIN DASHBOARD</h1>  --}}

<div class="p-4">

    <div class="row">
    
    
    
        <div class="col-4">
        <div class="info-box mb-3 bg-warning">
            <span class="info-box-icon"><i class="fas fa-shopping-basket"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Orders Today</span>
                <span class="info-box-number">{{$stats['new_orders']}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        </div>
        
        <div class="col-4">
        <div class="info-box mb-3 bg-danger">
            <span class="info-box-icon"><i class="fas fa-dollar-sign"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Total Revenue Today</span>
                <span class="info-box-number">{{$stats['today_sales']}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        </div>
    
    
        <div class="col-4">
        <div class="info-box mb-3 bg-success">
            <span class="info-box-icon"><i class="fas fa-cubes"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Total Items Sold</span>
                <span class="info-box-number">{{$stats['items_sold']}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        </div>
    

    
        
    </div>
    

@endsection