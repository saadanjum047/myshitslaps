@extends('layouts.admin')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">Manage Subscribers</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Create Date</th>
                            <th>Actions</th>
                        </tr>
                        @if(isset($subs))
                            @foreach($subs as $k => $sub)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> {{$sub->email}} </td>
                                    <td> {{$sub->created_at->format('m/d/Y') }} </td>
                                    <td class="text-align">


                                        <a href="javascript:;" onclick="AskBeforeDelete({{$sub->id}} , 'delete-form-{{$sub->id}}')" class="text-danger"  > <i class="fas fa-trash"></i> </a>
                                    </td>

                                    <form id="delete-form-{{$sub->id}}" action="{{route('admin.subscriber.destroy' , $sub->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </tr>
                            @endforeach
                        @endif
                    </table>

                    {{$subs->links()}}
                </div>
            </div>

        </div>
    </div>

@endsection