@extends('layouts.admin')


@section('content')


<div id="app">
    <product-form @if(isset($product)) :current_product="{{$product}}" @endif ></product-form>
</div>


@endsection