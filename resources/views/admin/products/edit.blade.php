@extends('layouts.admin')

@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title">Edit Category</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <form action="{{route('admin.category.update' , $category->id )}}" method="POST" enctype="multipart/form-data" >
                        @method('PATCH')
                        @include('layouts.partials.form_errors')

                        @csrf

                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="name" class="row float-right col-form-label ">Name:</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" placeholder="Category Name" name="name" value="{{old('name') ?? $category->name }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="description" class="row float-right col-form-label ">Description:</label>
                            </div>
                            <div class="col-sm-8">
                                <textarea class="textarea" name="description" id="description" placeholder="Description" rows="4" >{{old('description') ?? $category->description  }}</textarea>
                            </div>
                        </div>

                        <div>
                            <button id="signup" class="btn btn-primary float-right"><i class="fas fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
