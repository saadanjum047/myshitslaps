@extends('layouts.admin')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">Manage Products</h3>
                    <a class="btn btn-outline-primary btn-sm float-right" href="{{route('admin.products.create')}}"> <i class="fas fa-plus"></i> Create</a>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Create Date</th>
                            <th>Actions</th>
                        </tr>
                        @if(isset($products))
                            @foreach($products as $k => $product)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> {{$product->title}} </td>
                                    <td> {{ $product->is_free ? 'Free' : '$'. $product->price}} </td>

                                    <td> {{$product->created_at->format('m/d/Y') }} </td>
                                    <td>


                                        <a href="javascript:;" onclick="AskBeforeDelete({{$product->id}} , 'delete-form-{{$product->id}}')" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                                        <a href="{{ route('view-product' , $product->slug) }}" class="text-success"  target="_blank" > <i class="fas fa-eye"></i> </a>

                                        <a href="{{route('admin.products.edit' , $product->id)}}" > <i class="fas fa-edit"></i> </a>
                                    </td>

                                    <form id="delete-form-{{$product->id}}" action="{{route('admin.products.destroy' , $product->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </tr>
                            @endforeach
                        @endif
                    </table>

                    {{$products->links()}}
                </div>
            </div>

        </div>
    </div>

@endsection