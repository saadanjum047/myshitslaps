@extends('layouts.admin')

@push('style')


@endpush

@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title">Add New Category</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <form action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data" >
                        @include('layouts.partials.form_errors')

                        @csrf

                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="name" class="row float-right col-form-label ">Name:</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" placeholder="Category Name" name="name" value="{{old('name')}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="description" class="row float-right col-form-label ">Description:</label>
                            </div>
                            <div class="col-sm-8">
                                <textarea class="textarea" name="description" id="description" placeholder="Description" rows="4" >{{old('description')}}</textarea>
                            </div>
                        </div>

                        <div>
                            <button id="signup" class="btn btn-primary float-right">Create</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
