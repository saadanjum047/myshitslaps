<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>



    <style>
        #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #666666;
          color: white;
        }

        .fafoo{
            margin:10px;
            text-decoration: none;
            color: #666666;
        }
        .text-dark{
            text-decoration: none;
        }
        .logo-heading{
          font-size: 25px;
          font-weight: bolder;
          font-style: italic;
          font-family: Integral_C;
        }
        </style>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>

  
  {{--  @php
    $msg = 'NONE';
    $order = App\Order::first();
  @endphp  --}}

    <div style="width: 100%; text-align: center; margin-top:20px; ">

      {{--  <div class="logo-heading">My Shit Slaps.</diiv>  --}}
        <img src="{{ asset('images/logo.png') }}" />
    </div>

    
    <div style="width: 80%; margin-left: 10%; margin-top:50px; min-height: 340px; " >
        
        {!! $msg !!}

        <h3>Order Details</h3>
        
        <table id="customers" style="margin-top: 20px; margin-bottom: 40px;" >
        <tr>
          <th>#</th>
          <th>Product Name</th>
          <th>Price</th>
          <th style="max-width: 100px;" >Link</th>
        </tr>
        @foreach($order->items as $k => $item)
        <tr @if($loop->last) style="border-bottom: 2px solid #666666;" @endif  >
          <td>{{$item->k}}</td>
          <td>{{$item->title}}</td>
          <td> {{ $item->is_free ? "Free" : '$'. $item->price}} </td>
          <td> <a class="download-btn" href="{{ url('/') . '/request-file/' . $item->id . '/id/'. $item->download_token }}" > Download File</a></td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td style="text-align: right" > Total </td>
            <td>$ {{ array_sum ($order->items->pluck('price')->toArray()) }} </td>
          </tr>
          
      </table>
    </div>



    <hr>
    <div style="width: 100%; text-align: center ">

    <div class="">
        <div class="row">
          <div class="col-md-12 text-center">
      <img src="{{asset('assets/images/vv.jpg')}}" class="img-fluid pb-3" style="width: 80px;" alt="">
            
            <div class="text-center mt-2">
             <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i>
      
       </a>          <a class="text-dark" href="">
      <i class="fa fa-instagram fafoo"></i>
       </a> 
                <a class="text-dark" href="">
      <i class="fa fa-twitter fafoo"></i>
       </a>      </div>
            <h4 class="text-center mt-3 fh4  color_grey">&copy; MYSHITSLAPS. <br> All rights reserved</h4>
          </div>
        </div>
      </div>
    </div>



</body>
</html>

