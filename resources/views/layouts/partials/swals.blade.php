
<script>
  {{--  Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Sasd ',
      showConfirmButton: false,
      timer: 1500
    });  --}}

    {{--  Sweel("Good job!", "You clicked the button!", "success");  --}}

    {{--  Sweel("success!", "Success!", "success");  --}}

    {{--  Sweel("Warning!", "Warning!", "warning");  --}}
  
    {{--  Sweel("Error!", "Error!", "error");  --}}
  
    {{--  Sweel("Message!", "Info!", "info");  --}}

    
</script>

@if(session()->has('success'))
<script>
  Sweel("success!", "{{session('success')}}!", "success");

    {{--  Swal.fire({
        position: 'center',
        icon: 'success',
        title: '{{session("success")}} ',
        showConfirmButton: false,
        timer: 1500
      });  --}}
</script>
@endif

@if(session()->has('unauthorized'))
<script>
  Sweel("Warning!", "{{session('unauthorized')}}!", "warning");
  {{--  Swal.fire({
        position: 'center',
        icon: 'info',
        title: '{{session("unauthorized")}} ',
      });  --}}
</script>
@endif
@if(session()->has('deactivated'))
<script>
  Sweel("Error!", "{{session('deactivated')}}!", "error");
</script>
@endif
@if(session()->has('error'))
<script>
  Sweel("Error!", "{{session('error')}}!", "error");

    {{--  Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: '{{session("error")}} ',
        showConfirmButton: false,
        timer: 1500
      });  --}}
</script>
@endif
@if(session()->has('message'))
<script>
  Sweel("Message!", "{{session('message')}}!", "info");

    {{--  Swal.fire({
        position: 'center',
        icon: 'info',
        title: '{{session("message")}} ',
        showConfirmButton: false,
        timer: 1500
      });  --}}
</script>
@endif

