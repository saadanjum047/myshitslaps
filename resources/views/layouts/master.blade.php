<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="viewport" content="width = 1050, user-scalable = no" />

    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->

    <meta content="width=device-width, initial-scale=1" name="viewport" />
    {{--  <script src="https://unpkg.com/wavesurfer.js"></script>  --}}

    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> --}}

    {{--  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">  --}}

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


    {{--  <link rel="stylesheet" href="http://localhost/shop/assets/styles">  --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">

    <script
        src="https://www.paypal.com/sdk/js?client-id=AXtq-_Ptkt8tXoO3aragM4UM8VJx4Vn6jjtO8xunIjRRQTGEkI7xsLzaF2aSvIWrdt1YxGPdt6rSRUQQ&disable-funding=credit,card"> // Required. Replace YOUR_CLIENT_ID with your sandbox client ID.
    </script>

    <title>@yield('title')</title>
    <style>

        .toast.toast-info{
            background-color: #000 !important;
        }
    </style>

    @yield('styles')
    
</head>
<body>

    

<div class="products-home" id="app">
    <div class="shop-navbar">
        <div class="heading-box">
            <div class="heading-center">

                <i class="fas fa-bars mobile-menu-icon-btn" onclick="sidebar_open()"></i>
                <a href="/">
                    My Shit Slaps.
                </a>
                
                {{--  <i class="fal fa-cart-plus mobile-cart-btn" onclick="window.location = '/cart'"></i>  --}}
                <div class="mobile-cart-btn">
                    <div class="total-products cart-items-count"  @if(!session()->has('cart.items') ) style="display: none;" @endif id="cart-items" >{{ session('cart.items') ? count (session('cart.items')) : 0 }}</div>
                    <i class="fal fa-cart-plus " onclick="window.location = '/cart'"></i>
                </div>
                

            </div>
        </div>
        <div class="menu-links">
            
            @php
                $categories = App\Category::all();
            @endphp

            <div class="menu-link dropdown-link"><a class="{{strpos(url()->current() , '/category') ? 'active' : ''}}" href="/" >Products</a>
                <div class="menu-dropdown">
                    <a href="{{route('category-products' , 'all')}}" class="dropdown-item">ALL</a>    
                    @foreach ($categories as $category)
                        <a href="{{route('category-products' , $category->slug)}}" class="dropdown-item">{{$category->name}}</a>
                    @endforeach
                    {{--  <a href="#" class="dropdown-item">LOOP PACK</a>    
                    <a href="#" class="dropdown-item">PRESETS</a>    
                    <a href="#" class="dropdown-item">MDI</a>      --}}
                </div>
            </div>

            {{--  @dd(session('cart.items'))  --}}

            <div class="menu-link dropdown-link"><a class="{{strpos(url()->current() , '/faqs') || strpos(url()->current() , '/contact-us') || strpos(url()->current() , '/license-agreement') ? 'active' : ''}}"  href="#" >Support</a>
                <div class="menu-dropdown">
                    <a href="/faqs" class="dropdown-item">FAQs</a>    
                    <a href="/contact-us" class="dropdown-item">Contact</a>    
                    <a href="/license-agreement" class="dropdown-item">License Agreement</a>    
                </div>
            </div>
            <div class="menu-link"><a class="{{strpos(url()->current() , '/free-stuff') ? 'active' : ''}}" href="{{route('free-products')}}">Free Stuff</a></div>

            <div class="menu-link position-relative"><a class="{{strpos(url()->current() , '/cart') ? 'active' : ''}}" href="/cart">Cart </a>
                
                <div class="total-products cart-items-count"  @if(!session()->has('cart.items') ) style="display: none;" @endif id="cart-items" >{{ session('cart.items') ? count (session('cart.items')) : 0 }}</div>
            </div>
            
            
            
        </div>
    </div>


    <div class="sidebar" id="mySidebar">
        <i class="fas fa-times close-btn" onclick="sidebar_close()"></i>
        <div class="mobile-menu">
            <div class="menu"> <a data-toggle="collapse" href="#product_categories" role="button" aria-expanded="false" aria-controls="product_categories">Products</a> </div>
                <div class="collapse" id="product_categories">
                    <div class="sub-menu"> <a href="{{route('category-products' , 'all')}}" >ALL</a> </div>
                    @foreach ($categories as $category)
                        <div class="sub-menu"> <a href="{{route('category-products' , $category->slug)}}">{{$category->name}}</a></div>
                    @endforeach
                </div>

            <div class="menu"> <a data-toggle="collapse" href="#support_dropdown" role="button" aria-expanded="false" aria-controls="support_dropdown" >Support</a> 
            </div>
                <div class="collapse" id="support_dropdown">
                    <div class="sub-menu"> <a href="{{ route('faqs') }}">FAQs</a> </div>
                    <div class="sub-menu"> <a href="{{route('contact-us')}}">Contact</a> </div>
                    <div class="sub-menu"> <a href="{{route('license-agreement')}}">License Agreement</a> </div>
                </div>

            <div class="menu"> <a href="{{route('free-products')}}">Free Stuff</a> </div>
            <div class="menu"> <a href="/cart">Cart</a> </div>

        </div>
    </div>

    <div class="wrapper" >
        @yield('content')
    </div>


    <div class="footer">
        
        
        <div class="footer-links">
            <a href="/">Home</a>
            <a href="{{route('faqs')}}">FAQs</a>
            <a href="{{route('contact-us')}}">Contact</a>
        </div>
        <div class="social-icons">
            <a href="#"><i class="fab fa-youtube"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-soundcloud"></i></a>
        </div>
        <div class="mail-area">
            <mailing-component></mailing-component>
        </div>
    </div>
</div>
</body>

<script src="https://js.stripe.com/v3/"></script>
<script>
 let stripe = Stripe(`pk_test_vZWip3YSKcRkneT1fqhNN28c00garADMH0`),
 elements = stripe.elements(),
 card = undefined;
</script>

{{-- <script src="https://unpkg.com/wavesurfer.js"></script> --}}

<script src="{{asset('js/app.js')}}" ></script>

<script>
    @if(session()->has('error'))
        toastr.error( ' {{session('error')}}' );
    @endif
    @if(session()->has('info'))
        toastr.info( ' {{session('info')}}' );
    @endif

    function sidebar_open() {
        document.getElementById("mySidebar").style.width = "75%";
      }
      
    function sidebar_close() {
        document.getElementById("mySidebar").style.width = "0";
    }
    </script>
      

    <script>
    
        {{-- function pausePlay(){
            wavesurfer.playPause();
    
            if(wavesurfer.isPlaying()){
                playing();
            }else{
                paused();
            }
        }
    
        function playing(){
            $('#audio-icon').removeClass('fa-play');
            $('#audio-icon').addClass('fa-pause');        
        }
    
        function paused(){
            $('#audio-icon').addClass('fa-play');
            $('#audio-icon').removeClass('fa-pause');        
        } --}}
    
    </script>
@yield('scripts')

</html>