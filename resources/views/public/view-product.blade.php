@extends('layouts.master')

@section('title' , 'View Product')

@section('content')


<view-product :product="{{$product}}" :related_products="{{$related_products}}" ></view-product>


@endsection