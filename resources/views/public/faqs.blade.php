@extends('layouts.master')

@section('styles')
    <style>
        .faqs .single-faq{
            border: 2px solid #a7a7a7;
            border-radius: 5px;        }
        .faqs{
            padding: 20px 65px;
        }

        @media screen and (max-width:768px){
            .faqs{
                padding: 15px;
            }
            .list .list-item{
                padding: 10px;
            }
            .list .expandable-area{
                padding: 10px;
            }
        }
    </style>
@endsection

@section('title' , 'FAQs')

@section('content')

<div class="">
    <div class="faqs">
        <div class="faqs-lists list">
            <div class="single-faq">
                
                <div class="list-item"><a  href="#" class="accordian-link collapsed" data-toggle="collapse" data-target="#accordian-1">
                    <div class="list-item-name">
                        What is the My Shit Slaps? 
                    </div>
                    
                    <div class="list-item-icon">
                        <i class="fas fa-angle-down arrow-down-right"></i>
                    </div>
                    </a>
                </div>
            
                <div id="accordian-1" class="collapse">
                    <div class="expandable-area">
                        The market place 
                    </div>
                </div>
            
            
                
            </div>
        </div>
    </div>
</div>



@endsection