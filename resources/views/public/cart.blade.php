@extends('layouts.master')


@section('title' , 'Cart')

@section('content')

<cart :cart_items="{{ json_encode( $products) }}"></cart>

@endsection