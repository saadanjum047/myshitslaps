@extends('layouts.master')

@section('title' , 'Checkout')

@section('content')

<checkout :cart_items="{{ json_encode( $products) }}"></checkout>

@endsection