@extends('layouts.master')

@section('title' , 'Download File')

@section('content')
    

<div class="page">
    <div class="box-container">
        <div class="box">
            <div class="page-title text-center">
                
                @if($token != $item->download_token)
                <i class="fas fa-info-cirlce mr-2"></i>  Token not matched!. Please uses the correct link to download
                @elseif($item->downloaded_times >= 5)
                <i class="fas fa-info-cirlce mr-2"></i>  We are sorry to inform you that your Download limit has been finished
                <br>
                Click <a href="/">here</a> to shop products
                @else
                    Hi, {{ ucwords($item->order->first_name . ' ' . $item->order->last_name) }}, Your file is being prepare for download.
                    <br>
                    @if(5 - ($item->downloaded_times + 1) > 0)
                    You can download this file {{ 5 - ($item->downloaded_times + 1)}} more times from the same link
                    @else
                    Your download limit has been completed. You will not be able to download from this link again.
                    @endif
                    <br>
                    <i class="fas fa-info-cirlce mr-2"></i> Thank You
                    <form id="download-form" method="POST" action="{{url('request-file-dwonload')}}">
                        @csrf
                        <input type="hidden" name="download_token" value="{{ $item->download_token }}" />
                        <input type="hidden" name="download_item" value="{{ $item->id }}" />
                    </form>
                    
                    <a id="download-btn" href="javascript:;" onclick="window.location.reload(true)" class="m-btn proceed-btn mb-2">
                        <loader font_size="14px"></loader> Your File is processing ...
                    </a>
                @endif


            </div>
        </div>

    </div>
</div>

@endsection


@section('scripts')
    
<script>
    $(document).ready(function(){
        setTimeout(function(){
            $('#download-form').submit();
            setTimeout(() => {
                $('#download-btn').html('Dowload Not started ? Try again')
            },2000)
        } , 1000)
        
    })
</script>
@endsection