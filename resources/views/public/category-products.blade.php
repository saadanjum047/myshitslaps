@extends('layouts.master')

@section('title' , 'Products')

@section('content')

<div class="content">
<div class="products">
    @foreach ($products as $product)    
        <div class="product-box">
            <a href="{{route('view-product' , $product->slug)}}">
                <div class="featured-image-box">
                    @if($product->thumbnail)
                    <img src="{{ asset('storage/products/'.$product->thumbnail)}}" class="featured-image" />
                    @else
                    <img src="/img/timthumb.png" class="featured-image" />
                    @endif
                </div>
                <div class="product-details">
                    <div class="product-title">{{$product->title}}</div>
                    @if($product->discounted_price && $product->discounted_price > 0)
                    <div class="product-price-with-discount">
                        <span class="original-price"> ${{$product->price}} </span>
                        <span class="discounted-price">${{ $product->discounted_price }}</span>
                    </div>
                        @else
                        <div class="product-price @if($product->is_free) free @endif">{{ $product->is_free ? 'Free' : '$'. $product->price }}
                            
                        </div>
                        @endif
                </div>
            </a>
        </div>
    @endforeach
    
    @if($products->count() == 0)
        <div class="no-products-found-message">
            <i class="fas fa-info-circle"></i> Sorry! No Products found
        </div>
    @endif

</div>
</div>

@endsection