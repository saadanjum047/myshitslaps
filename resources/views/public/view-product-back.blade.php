@extends('layouts.master')

@section('content')

<div class="single-product">
    
    <div class="description-container">
        <div class="description-box">
            <div class="product-title">
                {{$product->title}}
            </div>
            @if($product->discounted_price)
            <div class="product-price-box"> 
                <span class="old-price">${{$product->price}}</span>
                <span class="discounted-price ml-3">${{$product->discounted_price}}</span>
            </div>
            @else
            <div class="price">${{$product->price}}</div>
            @endif
            <div class="description">
                {{$product->description}}
            </div>

            <div class="preview-section">
                <div class="preview-text mb-2">Preview</div>

            <div class="preview-box">

                <div class="audio-container">

                    <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay"src="{{$product->preview_link}}"></iframe>
{{-- 
                    <iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay"src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/517244934&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe> --}}

                    <div style="font-size: 10px; color:#cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space:nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,LucidaSans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;">

                        <!-- <a href="https://soundcloud.com/knxwledge" title="Knx.[ノレッジ]" target="_blank" style="color:#cccccc; text-decoration: none;">Knx.[ノレッジ]</a> · 
                        <a href="https://soundcloud.com/knxwledge/dpb6lstmqtqy" title="[​確かにはい] ]" target="_blank"style="color: #cccccc; text-decoration: none;">[​確かにはい] ]</a> --></div>

                </div>
            </div>
            </div>

            <a href="#" class="add-to-cart">Add To Cart</a>
        </div>
    </div>



    <div class="image-container">
        <!-- <img src="https://images.pexels.com/photos/3413462/pexels-photo-3413462.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" /> -->

        @if($product->images->count() > 0)
        <img src="{{ asset('storage/products/'.$product->images[0]->name)}}" class="featured-image" />
        @else
        <img src="https://images.pexels.com/photos/45243/saxophone-music-gold-gloss-45243.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260" class="featured-image" />
        @endif
</div>
        

</div>





<div class="related-products-title integral-fonts">Related Products</div>
<div class="related-products">
   
    @if ($related_products)
    @foreach ($related_products as $product )
    <div class="related-product-box">
        <a href="/shop/product">
            <div class="featured-image">
                @if($product->images->count() > 0)
                <img src="{{ asset('storage/products/'.$product->images[0]->name)}}" class="featured-image" />
                @else
                <img src="https://images.pexels.com/photos/45243/saxophone-music-gold-gloss-45243.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260" class="featured-image" />
                @endif
            </div>
            <div class="product-details">
                <div class="product-title">{{$product->title}}</div>
                <div class="product-price">${{$product->price}}</div>
            </div>
        </a>
    </div>
    @endforeach
    @endif
    

</div>






@endsection