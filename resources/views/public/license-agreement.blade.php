@extends('layouts.master')

@section('styles')
    <style>
        .license-agreement-box{
            background-color: #d2d2d2;
            font-family: monospace;
            box-shadow: rgb(0 0 0 / 12%) 0 1px 3px;
            padding: 20px;
            border: 2px solid #a7a7a7;
            border-radius: 5px;
            color: #787878;
            font-size: 17px;
        }
        .license-agreement-container{
            padding: 20px 65px;
            margin-bottom: 20px;
        }

        @media screen and (max-width:768px){
            .license-agreement-container{
                padding: 15px;
            }
            .license-agreement-box{
                padding: 10px;
            }
        }

    </style>
@endsection

@section('title' , 'License Agreement')

@section('content')

<div class="">
    <div class="license-agreement-container">
        <div class="license-agreement-box">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis provident, voluptatum, nisi quibusdam ullam, architecto accusamus autem ducimus amet ab reprehenderit accusantium laboriosam cum! Debitis explicabo est totam laborum omnis!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis provident, voluptatum, nisi quibusdam ullam, architecto accusamus autem ducimus amet ab reprehenderit accusantium laboriosam cum! Debitis explicabo est totam laborum omnis!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis provident, voluptatum, nisi quibusdam ullam, architecto accusamus autem ducimus amet ab reprehenderit accusantium laboriosam cum! Debitis explicabo est totam laborum omnis!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis provident, voluptatum, nisi quibusdam ullam, architecto accusamus autem ducimus amet ab reprehenderit accusantium laboriosam cum! Debitis explicabo est totam laborum omnis!
        </div>
    </div>
</div>



@endsection