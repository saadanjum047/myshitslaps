@extends('layouts.auth.auth')

@section('title')
<a href="/"><b> MyShitSlaps </b>ADMIN</a>
@endsection

@section('content')

    <p class="login-box-msg">Sign in</p>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="input-group mb-3">
            <input type="email" class="form-control  @error('email') is-invalid @enderror input-fields" name="email" value="{{ old('email') }}" required placeholder="email">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>
            @error('email')
            <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
        <div class="input-group mb-3">
            <input type="password" class="form-control @error('password') is-invalid @enderror input-fields" name="password" required  placeholder="Password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
            @error('password')
            <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
            @enderror
        </div>
        <div class="row">
            <div class="col-8">

            </div>
            <!-- /.col -->
            <input type="hidden" name="role" value="1" />

            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <p class="mb-1">
        <a href="{{route('password.request')}}">I forgot my password</a>
    </p>

@endsection


