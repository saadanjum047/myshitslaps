<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('add-product' , 'Api\ProductController@store' );
Route::post('update-product/{id}' , 'Api\ProductController@update' );

Route::post('publish-product' , 'Api\ProductController@publishProduct' );

Route::get('download-file/{file_id}' , 'Api\ProductController@downloadFile' );
Route::post('download-file' , 'Api\ProductController@downloadFile' );
Route::post('delete-file' , 'Api\ProductController@deleteFile' );
// Route::get('download-file/{id}' , 'Api\ProductController@downloadFile' ) -> middleware('auth') ;

Route::post('product-images' , 'Api\ProductController@images' );
Route::post('delete-product-image' , 'Api\ProductController@deleteImages' );

Route::post('upload-product-file/{product_id}' , 'Api\FileController@uploadFile' );

Route::post('get-categories' , 'Api\ProductController@getCategoies' );

Route::post('send-order-details' , 'OrderController@sendOrderDetails' );


Route::post('add-to-cart' , 'Api\CartController@addToCart' );
Route::post('coupen-code-check' , 'Api\CheckoutController@coupenCodeCheck' );


Route::post('store/contact-us' , 'Api\ContactController@store' );
Route::post('subscribe' , 'AdminController@subscribe' );