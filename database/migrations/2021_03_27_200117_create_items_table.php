<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('order_id');

            $table->string('image')->nullable();
            $table->string('title');
            $table->unsignedBigInteger('category_id');
            $table->string('price')->default('0');
            $table->string('total_price')->nullable();
            $table->boolean('is_discount')->default(0);
            $table->boolean('is_free')->default(0);
            $table->text('description')->nullable();
            $table->unsignedInteger('downloaded_times')->default(0);
            $table->text('download_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
