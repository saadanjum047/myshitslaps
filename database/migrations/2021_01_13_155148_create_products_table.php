<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->boolean('published')->default(0) ;
            $table->unsignedBigInteger('category_id');
            $table->text('preview_link')->nullable();
            $table->string('price')->default('0');
            $table->string('discounted_price')->nullable();
            $table->string('thumbnail')->nullable();
            $table->text('description')->nullable();
            $table->text('sound')->nullable();
            $table->boolean('is_free')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
