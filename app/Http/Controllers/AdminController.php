<?php

namespace App\Http\Controllers;

use App\Order;
use Carbon\Carbon;
use App\Subscriber;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){

        $new_orders = Order::whereDate('created_at', Carbon::today())->get();
        $stats['items_sold'] = 0;
        foreach($new_orders as $order){
            $stats['items_sold'] += $order->items->count();
        }
        $stats['new_orders'] = Order::whereDate('created_at', Carbon::today())->count();
        $stats['today_sales'] = array_sum(Order::whereDate('created_at', Carbon::today())->get()->pluck('amount')->toArray());
        
    	return view('admin.index' , compact('stats'));
    }


    public function profile(){
        $profile = auth()->user();
    	return view('admin.profile.index' , compact('profile'));
    }

    public function updateProfile(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'email | required',
        ]);

        $profile = auth()->user();

        $profile->name = $request->name;
        $profile->email = $request->email;
        if($request->password){
            $profile->email = \Hash::make($request->password);
        }
        $profile->save();

        if($request->has('image')){

            $ext = $request->image->getClientOriginalExtension();
            
            $request->image->move(public_path().'/images/admin/' , $profile->id .'.'. $ext);
            $profile->photo = $profile->id .'.'. $ext;
            
            $profile->save();
        }



    	return redirect('/admin');
    }

    public function subscribe(Request $request){
        if($request->email){
            try{
                $sub = Subscriber::create([
                    'email' => $request->email
                ]);
                return response()->json(['success' => true]);
            }catch(\Exception $e){
                return response()->json(['success' => false , 'message' => $e->getMessage()],400);
            }
        }
        return response()->json(['success' => false , 'message' => 'No Email Found'],400);
    }

    public function getSubscriber()
    {
        $subs = Subscriber::latest()->paginate(20);

        return view('admin.subscribers' , compact('subs'));
    }
    public function deleteSubscriber ($id)
    {
        $sub = Subscriber::find($id);
        $sub->delete();

        return redirect()->back();
    }
}
