<?php

namespace App\Http\Controllers;

use App\Item;
use App\Order;
use App\Product;
use Carbon\Carbon;
use Stripe\Stripe;
use App\ProductFile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class OrderController extends Controller
{
   
    public function placeOrder(Request $request)
    {

        // dd($request->all() , session('cart.items') );
        
        if(session()->has('cart')){

            if($request->method == 'stripe' && isset($request->result)){
                try{
                // Stripe::setApiKey("sk_live_MFnEFHFrzQU4lNMNQeQ3cZYA");
                Stripe::setApiKey("sk_test_OYYUegdZV0dXPN7ws3MTg0F000Rw1RpJMN");
                // Token is created using Checkout or Elements!
                // Get the payment token ID submitted by the form:
                $token = $request->result['token']['id'];
                // dd( $request->result , $token);
                $charge = \Stripe\Charge::create([
                    'amount' => $request->total_amount * 100,
                    'currency' => 'usd',
                    'description' => 'Product Purchase',
                    'source' => $token,
                ]);
        
                } catch(\Exception $e) {
                    return response()->json(['success' => false , 'message' => $e->getMesasge()],422);
                }

            }
            
            $random_number = rand(111111,999999);
            
            $order = Order::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email ?? $random_number,
                'payment_details' => $request->result,
                'payment_method' => isset($request->method) ? $request->method : "Free",
                'link' => 'link',
                'total_amount' => $request->total_amount,
                'amount' => $request->amount,
                'coupen' => isset($request->coupen) ? $request->coupen : NULL,
            ]);


            foreach(session('cart.items') as $item){
                $item = Item::create([
                    'product_id' => $item['id'],
                    'order_id' => $order->id,
                    'title' => $item['title'],
                    'description' => $item['description'],
                    'is_free' => $item['is_free'],
                    'price' => $item['is_free'] ? 0 : ($item['discounted_price'] ? $item['discounted_price'] : $item['price']),
                    'total_price' => $item['is_free'] ? 0 : $item['price'],
                    'category_id' => $item['category_id'],
                    'image' => $item['images'] ? $item['images'][0]['name'] : '',
                    'download_token' =>  md5(Str::random(25)).sha1(time()),
                    ]);
                }

            }
            
        $data['msg'] = 'Thanks for shopping here' ;
        $data['order'] = $order;
        $data['from'] = 'My Shit Slaps.';
            
        $subject = "Thank You for purchasing";    
        
        if($order){
            session()->forget('cart');
            session()->forget('cart.items');

            try{
                \Mail::send( 'emails.order_details', $data, function ($message) use ($order) {
                    $message->to( $order['email'] , $order['first_name'] . ' ' .$order['last_name']);
                    $message->subject('Your Order Details');
                });
                return response()->json(['success' => true , 'message' => 'Order Place Successfully' ]);
            }catch (\Throwable $e) {

                return response()->json( ['success' => false , 'email_exception' => true , 'message' => $e->getMessage() ] , 422);
            }


        }else{
            return response()->json(['success' => false , 'message' => 'Order Place Successfully' ] , 422 );
        }
    }
    

    public function index(){
        
        // $orders = Order::orderBy('created_at' , 'desc')->active()->paginate(5);
        $orders = Order::orderBy('created_at' , 'desc')->where('is_deleted',0)->paginate(20);
        return view('admin.orders.index' , compact('orders'));
    }

    public function show($id){
        $order = Order::find($id);

        return view('admin.orders.view' , compact('order'));
    }



    public function destroy($id){
        $order = Order::find($id);
        $order->is_deleted = true;
        $order->deleted_time = Carbon::now();
        $order->save();
        
        return redirect()->back()->with('success' , 'Order removed');
    }



    public function requestDownloadFile($item_id , $token)
    {
        $item = Item::find($item_id);
        // dd($item);
        
        if($token && $item){
            return view('public.download-page' , compact('item' , 'token') );
        }else{

        }
    }

    public function downloadFile(Request $request)
    {
        $item = Item::find($request->download_item);


        $token = $request->download_token;

        if($token != $item->download_token){
            // token mismatch
            return redirect()->back() ->with('error' , 'Your download token does not match');
        }
        elseif($item->downloaded_times > 5){
            // download exceeded
            return redirect()->back()->with('error' , 'Your can not download more files with this link');
        }
        // Download Limit Completed
        else{


            $product = Product::find($item->product_id);
            // dd($product->file);
            
            $file = $product->file;
            $item->downloaded_times = $item->downloaded_times + 1;
            
            
            if($file && file_exists( public_path(). '/storage/products/files/'. $file->src) ){
                
                $item->save();
                
                return response()->download( public_path(). '/storage/products/files/'. $file->src);
    
            }else{
                return redirect('/')->with ('error' , 'File no longer exists');
            }
            
        }
        
        return redirect('/')->with('error' , 'Something unexpected happend');
        
    }


    public function resendEmail($id)
    {
        $order = Order::find($id);

        $data['msg'] = 'Thanks for shopping here' ;
        $data['order'] = $order;
        $data['from'] = 'My Shit Slaps.';

        \Mail::send( 'emails.order_details', $data, function ($message) use ($order) {
            $message->to( $order->email , $order->first_name . ' ' .$order->last_name);
            $message->subject('Your Order Details');
        });

        return redirect()->back();

    }

    public function getOrderDetails()
    {
        return view('public.get-order-details');
    }

    public function sendOrderDetails(Request $request)
    {

        $orders = Order::where( 'email' , $request->email)->where('is_deleted' , 0)->latest()->take(5)->get();
        // dd($orders);
        if($orders->count() > 0){
            foreach($orders as $order){
                $data['msg'] = 'Order Date: '.$order->created_at->toDateString() ;
                $data['order'] = $order;
                $data['from'] = 'My Shit Slaps.';
        
                \Mail::send( 'emails.order_details', $data, function ($message) use ($order) {
                    $message->to( $order->email , $order->first_name . ' ' .$order->last_name);
                    $message->subject('Your Order Details');
                });
            }
                return response()->json(['success' => true , 'message' => 'Order Details sent']);
        }else{
            return response()->json(['success' => false , 'message' => 'No orders found for this email'], 422);
        }
    }


}
