<?php

namespace App\Http\Controllers;

use App\Coupen;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CoupenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupens = Coupen::latest()->paginate(20);

        return view('admin.coupens.index' , compact('coupens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'discount' => 'required',
        ]);

        $coupen = Coupen::create([
            'name' => $request->name,
            'description' => $request->description,
            'validity' => $request->validity,
            'discount' => $request->discount,
            'code' => Str::random(10),
        ]);


        if($coupen){
            return redirect('/admin/coupen')->with('success' , 'Coupen Added successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function show(Coupen $coupen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupen $coupen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupen $coupen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupen  $coupen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupen $coupen)
    {
        $coupen->delete();
        return redirect('/admin/coupen')->with('success' , 'Coupen deleted successfully');
    }
}
