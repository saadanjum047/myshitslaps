<?php

namespace App\Http\Controllers;

use App\Contact;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QueryController extends Controller
{
    public function index()
    {
        $queries = Contact::latest()->paginate(20);
        return view('admin.queries.index' , compact('queries'));    
    }

    public function show($id)
    {
        $query = Contact::find($id);
        return view('admin.queries.view' , compact('query'));    

    }
    public function resolve($id)
    {
        $contact = Contact::find($id);

        $contact->resolved = 1;
        $contact->resolved_at = Carbon::now();
        $contact->save();

        return redirect()->back()->with('success' , 'Query resolved');

    }
}
