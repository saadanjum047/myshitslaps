<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function homepage(){

        $products = Product::latest()->get();
        
        return view('public.home' , compact('products'));
    }

    public function showProduct($slug){

        $product = Product::where('slug' , $slug)->first();
        $related_products = Product::where('id' , '!=' , $product->id)-> where('category_id' , $product->category_id)->latest()->limit(3)->get();
        return view('public.view-product' , compact('product' , 'related_products'));
    }

    public function showCategoryProduct($slug){

        if($slug == 'all'){
            $products = Product::latest()->get();;
        }else{
            $category = Category::with('products')->where('slug' , $slug)->first();
            $products = $category->products;
        }
        return view('public.category-products' , compact('products'));
    }


    public function showFreeProduct(){

        $products = Product::where('is_free' , 1 )->latest()->get();

        return view('public.category-products' , compact('products'));
    }

    public function showCart(){
        
        // dd(session('cart.items'));
        if(session()->has('cart')){
            $products = session('cart.items');
            if($products){
                return view('public.cart' , compact('products'));
            }else{
                return redirect('/')->with('error' , 'No Products in cart');
            }
        }else{
            return redirect('/')->with('error' , 'No Products in cart');
        }
    }
    public function contactUS(){
        
        return view('public.contact');
    }
    public function showFAQS(){
        
        // dd(session('cart.items'));
        return view('public.faqs');
    }
    public function licenseAgreement(){
        
        // dd(session('cart.items'));
        return view('public.license-agreement');
    }
    public function showCheckout(){
        if(session()->has('cart.items')){

            $products = session('cart.items');
            return view('public.checkout' , compact('products'));
        }else{
            return redirect()->back()->with('error' , 'No Products in cart');
        }
    }
}
