<?php

namespace App\Http\Controllers\Api;

use App\Images;
use App\Product;
use App\Category;
use App\ProductFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function getCategoies(){
        $categories = Category::all();
        return response()->json(['status' => true , 'categories' => $categories ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function images(Request $request){

        // dd($request->all());

        $image = $request->image;
        $name =  uniqid(true) . rand(99, 999) . "." . $image->getClientOriginalExtension();
        $image->storeAs('/public/products', $name);

        $new_image = Images::create([
            'product_id' => $request->product_id,
            'name' => $name,
        ]);

        return response()->json(['status' => 'Image Stored' ,'image' => $new_image ]);
    }

    public function deleteImages(Request $request){

        // dd($request->all());

        $image = Images::find($request->image_id);

        if($image && file_exists('storage/products/'.$image->name) && File::delete('storage/products/'.$image->name)){

            $image->delete();
            return response()->json(['status' => 'Image delete' ]);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Image Not Found'] , 422);
        }
    }


    public function store(Request $request)
    {
        // dd($request->all() );


        if($request->thumb_image){
            $image = $request->thumb_image;
            $name =  uniqid(true) . rand(99, 999) . "." . $image->getClientOriginalExtension();
            $image->storeAs('/public/products', $name);    
        }

        $has_discount = false;
        if(isset($request->is_free) && ($request->is_free === true || $request->is_free === 'true')){
            $has_discount = true;
        }


        $product = Product::create([
            'title' => $request->title,
            // 'slug' => str_slug($request->title),
            'price' => $has_discount ? 0 : $request->price,
            'discounted_price' => $has_discount ? 0 :  $request->discounted_price,
            'description' => $request->description,
            'preview_link' => $request->preview_link,
            'is_free' => $has_discount ? 1 : 0,
            'category_id' => $request->category_id,
            'thumbnail' => $name ?? NULL,
        ]);

        if($product){
            return response()->json(['status' => 'success' , 'product' => $product]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        
        $product = Product::find($id);
        
        if($request->thumb_image){
            $image = $request->thumb_image;
            $name =  uniqid(true) . rand(99, 999) . "." . $image->getClientOriginalExtension();
            $image->storeAs('/public/products', $name);

            $product->thumbnail = $name ?? NULL;
        }

        $has_discount = false;
        if(isset($request->is_free) && ($request->is_free === true || $request->is_free === 'true')){
            $has_discount = true;
        }

        $product->title = $request->title;
        $product->price = $has_discount ? 0 : $request->price;
        $product->discounted_price = $has_discount ? 0 : $request->discounted_price;
        $product->description = $request->description;
        $product->preview_link = $request->preview_link;
        $product->is_free = $has_discount ? 1 : 0;
        $product->category_id = $request->category_id;

        if($product->save()){
            return response()->json(['status' => 'success' , 'product' => $product]);
        }


    }

    
    public function publishProduct(Request $request)
    {
        
        $product = Product::find($request->product_id);
        
        $product->published = 1;

        if($product->save()){
            return response()->json(['status' => 'success' , 'product' => $product]);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function downloadFile( $file_id){
        
        $file = ProductFile::find($file_id);
        // $file = ProductFile::find($request->file_id);

        // dd( 'storage/products/files/'. $file->src , file_exists( public_path(). '/storage/products/files/'. $file->src));

        if($file && file_exists( public_path(). '/storage/products/files/'. $file->src) ){
            return response()->download( public_path(). '/storage/products/files/'. $file->src);
            // return Response::download(public_path(). '/storage/products/files/'. $file->src); 

        }else{
            return response()->json(['status' => 'failed' , 'message' => 'File not found'] , 422);
        }
        
    }
    public function deleteFile(Request $request){
        
        $file = ProductFile::find($request->file_id);
        // $file->delete();

        if($file && file_exists( public_path(). '/storage/products/files/'. $file->src)){
            $file->delete();
            return response()->json(['status' => 'success' , 'message' => 'File deleted successfully' ]);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'File not found'] , 422);
        }
        
    }


}
