<?php

namespace App\Http\Controllers\Api;

use App\Item;
use App\Order;
use App\Coupen;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{

    public function coupenCodeCheck(Request $request){

        $coupen = Coupen::where('code' , $request->coupen)->first();
        // dd(isset($coupen));

        if(isset($coupen)){
            if(isset($coupen) && $coupen->isValid){
                return response()->json(['message' => 'Coupen Found' , 'coupen' => $coupen ]);
            }else{
                return response()->json(['message' => 'Coupen Expired' , 'coupen' => $coupen ] , 422 );
            }
        }
        return response()->json(['message' => 'Coupen Not Found'] , 422 );
    }


    public function placeOrder(Request $request)
    {

        // dd($request->all() , session('cart.items'));

        if(session()->has('cart')){
            
            $order = Order::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'payment_details' => $request->result,
                'payment_method' => $request->method,
                'link' => 'link'
            ]);
            
            foreach(session('cart.items') as $item){
                Item::create([
                    'order_id' => $order->id,
                    'title' => $item['title'],
                    'description' => $item['description'],
                    'price' => $item['price'] ? $item['price'] : $item['discounted_price'],
                    'category_id' => $item['category_id'],
                    'image' => $item['images'] ? $item['images'][0]['name'] : '',
                    'download_token' =>  md5(Str::random(25)) ,
                ]);
            }
        }

        if($order){
            session()->forget('cart');
            session()->forget('cart.items');
            return response()->json(['success' => true , 'message' => 'Order Place Successfully' ]);

        }else{
            return response()->json(['success' => false , 'message' => 'Order Place Successfully' ] , 422 );
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
