<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function addToCart(Request $request){
        // Cart::addToCart($request->product);

        if(session()->has('cart') && session()->has('cart.items')){
            foreach(session('cart.items') as $cart_product){
                if($cart_product['id'] == $request->product['id']){
                    return response()->json(['message' => 'Product Already in cart'] , 422);
                }
            }
            
            session()->push('cart.items' , $request->product );
            return response()->json(['message' => 'Product Added Successfully' , 'session_count' => count(session('cart.items')) ]);
            
        }else{
            session()->push('cart.items' , $request->product );
            // Session::push('cart.items', 'developers');
            return response()->json(['message' => 'Product Added Successfully' , 'session_count' => count(session('cart.items') )]);
        }
        
        return response()->json(['message' => 'Unexpected Error'] , 422);
    }

    public function deleteCartItem(Request $request){
        
        // dd($request->all());


        if(session()->has('cart.items') ){
            $products = session('cart.items');

            foreach($products as $k => $item){
                if($products[$k]['id'] == $request->item['id']){
                    $key = $k;
                }
            }
            
            // dd($products);
            session()->forget('cart.items');

            unset($products[$key]);
            
            if(count($products) > 0){
                session()->put('cart.items' , array_values($products));
            }

            return response()->json(['message' => 'Product Removed' , 'session_count' => session('cart.items') ? count(session('cart.items')) : 0 ]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
