<?php

namespace App;

class Slug
{
    protected function createSlug(string $title): string
    {

        $slugsFound = $this->getSlugs($title);
        $counter = 0;
        $counter += $slugsFound;

        $slug = str_slug($title, $separator = "-", app()->getLocale());

        if ($counter) {
            $slug = $slug . '-' . $counter;
        }
        return $slug;
    }

    /**
     * Find same listing with same title
     * @param  string $title
     * @return int $total
     */
    protected function getSlugs($title): int
    {
        return Listing::select()->where('title', 'like', $title)->count();
    }

    
}
