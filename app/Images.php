<?php

namespace App;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $guarded = ['id'];
 
    protected static function boot()
    {
        static::deleting(function ($image) {
            if(file_exists('storage/products/'.$image->name)){
                File::delete('storage/products/'.$image->name);
            }
        });
        
        parent::boot();
    }

    
    public function product(){
        return $this->belongsTo(Product::class);
    }
}
