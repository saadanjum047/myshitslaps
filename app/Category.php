<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        static::created(function ($post) {
            $post->update(['slug' => $post->name]);
        });
        
        parent::boot();
    }


    public function products(){
        return $this->hasMany(Product::class)->latest();
    }


    public function setSlugAttribute($value)
    {
        if(static::whereSlug($slug = Str::slug($value))->exists())
        {
            if(static::whereSlug($slug)->get('id')->first()->id !== $this->id){
                $slug = $this->incrementSlug($slug);

                if(static::whereSlug($slug)->exists()){
                    return $this->setSlugAttribute($slug);
                }
            }
        }

        $this->attributes['slug'] = $slug;
    }

    /**
     * Increment slug
     *
     * @param   string $slug
     * @return  string
     **/
    public function incrementSlug($slug)
    {
        // Get the slug of the created post earlier
        $max = static::whereSlug($slug)->latest('id')->value('slug');

        if (is_numeric($max[-1])) {
            return preg_replace_callback('/(\d+)$/', function ($matches) {
                return $matches[1] + 1;
            }, $max);
        }

        return "{$slug}-2";
    }

}
