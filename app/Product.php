<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $with = ['images'];
    protected $guarded = ['id'];

    
    protected static function boot()
    {
        static::deleting(function ($product) {
            foreach($product->images as $image){
                $image->delete();
            }
        });

        static::created(function ($post) {
            $post->update(['slug' => $post->title]);
        });
        
        parent::boot();
    }



    public function images(){
        return $this->hasMany(Images::class);
    }

    public function file(){
        return $this->hasOne(ProductFile::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }


    public function setSlugAttribute($value)
    {
        if(static::whereSlug($slug = Str::slug($value))->exists())
        {
            if(static::whereSlug($slug)->get('id')->first()->id !== $this->id){
                $slug = $this->incrementSlug($slug);

                if(static::whereSlug($slug)->exists()){
                    return $this->setSlugAttribute($slug);
                }
            }
        }

        $this->attributes['slug'] = $slug;
    }

    /**
     * Increment slug
     *
     * @param   string $slug
     * @return  string
     **/
    public function incrementSlug($slug)
    {
        // Get the slug of the created post earlier
        $max = static::whereSlug($slug)->latest('id')->value('slug');

        if (is_numeric($max[-1])) {
            return preg_replace_callback('/(\d+)$/', function ($matches) {
                return $matches[1] + 1;
            }, $max);
        }

        return "{$slug}-2";
    }



}
