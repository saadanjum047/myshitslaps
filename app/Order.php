<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $casts = [
        'payment_details' => 'object',
        'coupen' => 'object',
    ];

    public function items(){
        return $this->hasMany(Item::class);
    }

    public function scopeActive($q){
        return $q->where('is_deleted', 0 );
    }
}
