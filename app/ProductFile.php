<?php

namespace App;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

class ProductFile extends Model
{
    protected $with = ['product'];
    protected $guarded = ['id'];
    protected $appends = ['proper_date_time'];

    protected static function boot()
    {
        static::deleting(function ($file) {
            if(file_exists(public_path(). '/storage/products/files/'. $file->src)){
                File::delete(public_path(). '/storage/products/files/'. $file->src);
            }
        });
        
        parent::boot();
    }



    public function getProperDateTimeAttribute(){
        return $this->created_at->toDateString() . ' | ' . $this->created_at->format('h:m A');
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
