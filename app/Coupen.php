<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Coupen extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at' , 'updated_at' , 'validity'];


    public function getIsValidAttribute(){
        if($this->validity > Carbon::now()){
            return true;
        }else{
            return false;
        }
            
    }

    public function getValidityDateAttribute(){

        if($this->validity){
            return $this->validity->toDateString();
        }
        
    }

}
